%% Example of RABASAR on Stack of 69 Sentinel-1 form Saclay

clear all
close all

init;

% Load simulated data
u = load('saclay_sentinel1_100looks.stack_with_simulated_changes.mat');
u = u.data;
[M, N, T] = size(u);

% Generate simulated iid speckle
L = 1;
v = u .* mean((randn(M, N, T, L).^2 + randn(M, N, T, L).^2) / 2, 4);

% Choose targated date
t = 11;

% Rabasar callibration (to be perfromed offline)
thrs = bw_thresholds(M, N, L, 7, .92);

% Rabasar (online)
%  - Notations follow the paper notations
h = robustwaitbar(0);
tic
[u_hat_t, u_hat_si, L_hat_si, u_hat_dsi, L_hat_dsi] = ...
    rabasar(v, L, t, ...
            'thrs', thrs, ...
            'waitbar', @(p) robustwaitbar(p, h));
toc
close(h);

% Visualization
fancyfigure;
subplot(2,3,1);
h = plotimagesar(u_hat_si, 'alpha', 2/3);
title(sprintf('Arithmetic mean: L=%.2f', L_hat_si));
subplot(2,3,2);
plotimagesar(u_hat_dsi, 'rangeof', h);
title(sprintf('Super image: L=%.2f', L_hat_dsi));
subplot(2,3,4);
plotimagesar(u(:,:,t), 'rangeof', h);
title(sprintf('Ground truth at date: t=%d', t));
subplot(2,3,5);
plotimagesar(v(:,:,t), 'rangeof', h);
title(sprintf('Noisy image at date: t=%d', t));
subplot(2,3,6);
plotimagesar(u_hat_t, 'rangeof', h);
title(sprintf('Rabasar result at date: t=%d', t));
linkaxes;
